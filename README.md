# UA Zen Demo

## Background

The UA Zen theme and the various site components to be styled with it need additional modules and settings to make them work in the context of a Drupal-based web site. UA Zen Demo is a Dupal install profile and some drush make files that make it easy to bring up a test site using the theme. It installs the standard contributed modules and libraries, plus the custom content types and other parts of the site that are wrapped up as Features.

## Requirements

A web development environment that would support a manual installation of Drupal is needed (so a web server, with support for a recent version PHP, and a database). A recent version of drush (the extra command-line Drupal utility) must be working; it will now include the `drush make` command without needing any extras. Git is needed to download several of the features and libraries: some of the downloads will need cached SSH credentials to the uabrandingdigitalassets team area on Bitbucket. A working version of the Compass tool is needed to re-generate the theme's CSS files from the Sass .scss files: this in turn needs a Ruby environment.

## Installation

Decide on the location of a new Drupal installation root directory, known to your webserver; the example here will use `/var/www/uazendemoroot`. Clone or download a copy of the UA Zen Demo repository, `cd` into this, so the `distro_uazendemo.make` file appears directly in the current directory, and run drush make with this and the new installation root directory, for example
```
drush make distro_uazendemo.make /var/www/uazendemoroot
```
You should see a long list of confirmation messages, starting with “Beginning to build distro_uazendemo.make.”, listing the various standard Drupal contributed modules as they are downloaded, and also listing the ua_zen and ua_content_types theme and feature from Bitbucket. If no Drupal installation root directory gets created, check that you have rights to create subdirectories in the parent level of the directory tree (so in `/var/www` in the example). If the installation starts reasonably, but fails for ua_zen or ua_content_types, check that you can see the protected uabrandingdigitalassets repositories on Bitbucket.

Now do everything you would normally do to prepare a fresh Drupal download for installation; create a new database, make sure the default.settings.php is copied to a settings.php writable by the web server, the files directory is set up with the correct permissions, and so on. Note that it will shortly be necessary to re-generate the CSS files for the UA Zen theme. The installation directory (relative to the Drupal installation root) is `sites/all/themes/custom/ua-zen` and if the CSS files are missing from the subdirectories of this, the command to regenerate them from the Sass .scss files is `compass compile`. If this complains about not having Compass installed, try the command `bundle install` first. If this doesn't work try `gen install bundle` and try again. If this in turn doesn't work, check your Ruby setup.

Finally, make a new subdirectory called uazendemo within the profiles directory under the Drupal installation root and copy all the `uazendemo.*` files from the repository into that subdirectory (in practice it might be easiest to just copy or move the entire uazendemo directory that you checked out or downloaded). So profiles should have the usual `minimal`, `standard` and `testing` subdirectories, but also a new subdirectory `uazendemo`.

Now complete the normal installation of a new Drupal site, but notice that it will be using the new uazendemo profile, not the standard one, and should pre-load the modules, features and libraries. After the site is working, with a database connection, User 1 set up with a name and password, and so on, there is a bug that overrides some of the desired contents of the Features. With the current directory set to the new site directory, the command
```
drush fra -y
```
should remove most of the overrides.